#include "playersettings.h"
#include <QMutex>
#include <QApplication>
#include <QDesktopWidget>
#include <QDebug>
#include <QFile>
#include <QRect>

PlayerSettings* PlayerSettings::_instance = NULL;
static QMutex _imutex;

/**
 * @brief PlayerSettings::PlayerSettings
 */
PlayerSettings::PlayerSettings()
{
    _columnCount = 6;
    _rowCount = 6;
    _screenNumber = 0;
    _playerName = "player001";
    _playerId = 1;

    // default
    this->_loadOpt = 0;
    this->_beginId = 0;
    this->_beginFailedId = 0;

    this->_totalStreams = {743,742,741,740,739,738,737,735,734,733,732,731,730,729,728,727,726,725,724,723,722,721,720,719,718,717,716,715,714,712,711,710,708,707,706,705,703,702,701,699,698,697,696,695,694,693,692,691,689,688,687,686,685,684,683,681,680,678,675,674,673,672,671,670,669,668,667,666,665,664,663,662,661,660,659,658,657,656,655,654,652,651,650,649,648,647,646,645,644,643,642,641,640,639,638,637,636,635,634,632,631,630,629,628,627,626,625,624,623,622,621,620,619,618,617,616,615,614,613,612,611,610,609,608,607,606,605,604,603,602,601,600,599,598,597,596,595,594,593,592,591,590,589,588,587,586,585,584,583,582,581,580,578,577,576,575,574,573,572,570,569,568,567,566,565,564,563,562,561,560,559,558,557,556,555,554,553,552,551,550,549,548,547,546,545,544,543,542,541,540,539,538,537,536,535,534,533,532,531,530,529,528,527,526,525,524,522,521,520,519,518,517,516,515,512,511,510,509,508,507,506,505,504,501,500,496,495,494,493,492,491,489,487,483,482,481,480,479,478,477,476,475,474,473,472,471,470,469,468,467,466,465,464,463,462,461,460,459,457,456,455,454,453,452,451,450,449,448,447,446,445,444,443,442,441,439,438,437,436,435,434,433,432,431,430,429,427,426,425,424,422,420,419,418,417,416,415,414,413,412,411,410,408,407,406,405,404,403,402,401,400,399,398,397,396,395,394,393,392,391,390,389,388,387,386,385,384,383,382,381,380,379,378,377,376,375,374,373,372,371,370,369,368,367,366,365,364,363,362,361,360,359,358,357,355,353,352,349,348,347,346,345,344,343,342,341,340,338,336,335,334,333,332,331,330,329,328,327,326,325,324,322,321,320,319,318,317,316,314,313,312,311,310,309,307,306,305,303,302,301,299,298,297,296,295,294,293,292,291,290,289,288,287,286,285,283,282,281,280,279,278,277,276,275,274,273,272,271,270,269,268,267,266,265,264,263,262,261,260,259,258,257,256,255,254,253,252,251,250,249,248,247,246,245,243,242,241,240,239,238,237,236,235,234,233,232,231,230,229,228,227,226,225,224};

    // master & backup svr
    this->_masterServer = "rtsp://127.0.0.1:8554/";
    this->_backupServer = "rtsp://127.0.0.1:8554/";

    // format url
    for (int i=0; i< _columnCount*_rowCount; i++)
    {
        // _playerUrls.push_back("rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov");
        _playerUrls.push_back(_masterServer + QString::number(_totalStreams[i + _beginId]));
    }

    // load
    loadNativeStreams();

    load();

    // reload config
    this->_screenRect = QApplication::desktop()->screenGeometry(this->_screenNumber);

}

/**
 * @brief PlayerSettings::getInstance
 * @return
 */
PlayerSettings &PlayerSettings::getInstance()
{
    QMutexLocker _lock(&_imutex);

    if (PlayerSettings::_instance == NULL)
        PlayerSettings::_instance = new PlayerSettings();

    return  *PlayerSettings::_instance;
}

/**
 * @brief PlayerSettings::getStreamCount
 * @return
 */
int PlayerSettings::getStreamCount()
{
    return getRowCount() * getColumnCount();
}

/**
 * @brief PlayerSettings::getRowCount
 * @return
 */
int PlayerSettings::getRowCount()
{
    return this->_rowCount;
}

/**
 * @brief PlayerSettings::getColumnCount
 * @return
 */
int PlayerSettings::getColumnCount()
{
    return this->_columnCount;
}

/**
 * @brief PlayerSettings::getScreenId
 * @return
 */
int PlayerSettings::getScreenId()
{
    return this->_screenNumber;
}

/**
 * @brief PlayerSettings::getScreenWidh
 * @return
 */
int PlayerSettings::getScreenWidh()
{
    return _screenRect.width();
}

/**
 * @brief PlayerSettings::getScreenHeight
 * @return
 */
int PlayerSettings::getScreenHeight()
{
    return _screenRect.height();
}

/**
 * @brief PlayerSettings::getPlayerID
 * @return
 */
QString PlayerSettings::getPlayerName()
{
    return this->_playerName;
}

/**
 * @brief PlayerSettings::getStream
 * @param count
 * @return
 */
QString PlayerSettings::getStream(int count)
{
    if (count < this->_playerUrls.size())
    {
        return _playerUrls[count];
    }

    return QString();
}

void PlayerSettings::setStreams(const QList<int> streams)
{
    // format url
    _playerUrls.clear();

    for (int i=0; i< streams.count(); i++)
    {
        if (this->_loadOpt == 1)
        {
            _playerUrls.push_back(_masterServer + QString::number(streams[i]));
        }
        else
        if (this->_loadOpt == 2)
        {
            qDebug() << i << ":" << this->_nativeStreams[streams[i]];

            _playerUrls.push_back(this->_nativeStreams[streams[i]]);
        }
    }

}

/**
 * @brief PlayerSettings::load
 */
void PlayerSettings::load()
{
    qDebug() << "App path : " << QApplication::instance()->applicationDirPath();
    QString configFile = QApplication::instance()->applicationDirPath() + "/config";

    if (!QFile::exists(configFile))
    {
        PlayerSettings::store();
    }

    QSettings config(configFile, QSettings::IniFormat);
    this->_playerName = config.value("name").toString();
    this->_playerId = config.value("id").toInt();
    this->_rowCount = config.value("rows").toInt();
    this->_columnCount = config.value("columns").toInt();
    this->_screenNumber = config.value("screen").toInt();

    this->_loadOpt = config.value("stream_option").toInt();
    this->_beginId = config.value("begin_stream").toInt();
    this->_beginFailedId = config.value("begin_backup_stream").toInt();
    this->_masterServer = config.value("main-stream").toString();
    this->_backupServer = config.value("backup-stream").toString();

    _playerUrls.clear();

    if (this->_loadOpt == 1)
    {
        for (int i=0; i<= _columnCount*_rowCount; i++)
        {
            _playerUrls.push_back(this->_masterServer + QString::number(_totalStreams[i + this->_beginId]));
            qDebug() << this->_masterServer + QString::number(_totalStreams[i + this->_beginId]);
        }
    }
    else
    if (this->_loadOpt == 2) // native stream
    {
        for (int i=0; i<= _columnCount*_rowCount; i++)
            _playerUrls.push_back(this->_nativeStreams[_totalStreams[i + this->_beginId]]);
    }
    else
    {
        this->_playerUrls = config.value("urls").toStringList();

        // DEBUG
        qDebug() << _playerUrls.count();
        qDebug() << _playerUrls;
    }
}

/**
 * @brief PlayerSettings::store
 */
void PlayerSettings::store()
{
    QString configFile = QApplication::instance()->applicationDirPath() + "/config";

    QSettings config(configFile, QSettings::IniFormat);
    config.setValue("screen", this->getScreenId());
    config.setValue("rows", this->getRowCount());
    config.setValue("columns", this->getColumnCount());
    config.setValue("urls", this->_playerUrls);
    config.setValue("name", this->_playerName);
    config.setValue("id", this->_playerId);
    config.setValue("stream_option", this->_loadOpt);
    config.setValue("begin_stream", this->_beginId);
    config.setValue("begin_backup_stream", this->_beginFailedId);
    config.setValue("main-stream", this->_masterServer);
    config.setValue("backup-stream", this->_backupServer);


    config.sync();
}

void PlayerSettings::loadNativeStreams()
{
    QFile nativeStreamFile("streams.lst");
    if (!nativeStreamFile.open(QIODevice::ReadOnly | QIODevice::Text))
       return;

    while (!nativeStreamFile.atEnd())
    {
        QByteArray line = nativeStreamFile.readLine();

        if (line.length() > 0)
        {
            QList<QByteArray> tokens = line.split('\t');
            if (tokens.count() == 2)
            {
                int id = tokens[0].toInt();
                QString url = tokens[1].remove(tokens[1].length()-1, 1).toStdString().c_str();

                this->_nativeStreams[id] = url;

                qDebug() << id << ":" << url;
            }
        }

    }
}
