#ifndef HTTPCALLER_H
#define HTTPCALLER_H

#include <QObject>
#include <QList>

class HTTPCaller : public QObject
{
    Q_OBJECT
public:
    explicit HTTPCaller(QObject *parent = nullptr);

signals:
    void updateCameraList(const QList<int>);

public slots:
    void onGetCameraList(int screenId);

private slots:
    void onServerReseponse(const QVariant& result);
    void onServerReseponseError(const int &error, const QString &message);

};

#endif // HTTPCALLER_H
