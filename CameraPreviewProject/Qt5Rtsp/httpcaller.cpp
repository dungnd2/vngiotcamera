#include "httpcaller.h"
#include "http/bhttpclient.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>

HTTPCaller::HTTPCaller(QObject *parent) : QObject(parent)
{

}

/**
 * @brief HTTPCaller::onGetCameraList
 * @param screenId
 *
 * http://172.26.12.9:8080/iot/be/api/camera/native?cm=get_list_cam_on_screen&dt={"size":20, "screen_id":1}
 */
void HTTPCaller::onGetCameraList(int screenId)
{
    BHttpClient* httpInvoker = new BHttpClient(QString(""),this);
    QObject::connect(httpInvoker, SIGNAL(done(QVariant)), this, SLOT(onServerReseponse(QVariant)), Qt::UniqueConnection);
    QObject::connect(httpInvoker, SIGNAL(error(int,QString)), this, SLOT(onServerReseponseError(int, QString)), Qt::UniqueConnection);

    QString linkapi = "http://172.26.12.9:8080/iot/be/api/camera/native?"; // link api

    // set cmd
    httpInvoker->setUrl(QUrl(linkapi));
    httpInvoker->addParameter("cm", "get_list_cam_on_screen", true);

    // add data values
    QJsonObject jsonObj;
    jsonObj["size"] = 36;
    jsonObj["screen_id"] = screenId;
    QJsonDocument jsonDoc(jsonObj);
    httpInvoker->addParameter("dt",QString::fromUtf8(jsonDoc.toJson().data()).remove('\n'));

    // Do Get
    httpInvoker->process(GET);
}

/**
 * @brief HTTPCaller::onServerReseponse
 * @param result
 *
 * {
    "err": 0,
    "msg": "success",
    "dt": {
        "cam_ids": [
            743,
            742,
            741,
            740,
            739
        ]
    }
   }
 *
 */
void HTTPCaller::onServerReseponse(const QVariant &result)
{
    BHttpClient* httpClient = qobject_cast<BHttpClient*>(sender());
    httpClient->deleteLater();

    qDebug() << "Server Response in (ms): " << httpClient->getExcutionTime() << ", data: " <<  result;
    QJsonDocument   jsd = QJsonDocument::fromJson(result.toString().toUtf8());
    QJsonObject     jso = jsd.object();
    QJsonObject     dto = jso["dt"].toObject();

    QJsonArray jscreenArray = dto["cam_ids"].toArray();
    QVariantList camIdsVar = jscreenArray.toVariantList();
    QList<int> camIds;

    foreach(QVariant v, camIdsVar) {
        camIds << v.value<int>();
    }

    emit updateCameraList(camIds);

}

/**
 * @brief HTTPCaller::onServerReseponseError
 * @param error
 * @param message
 */
void HTTPCaller::onServerReseponseError(const int &error, const QString &message)
{
    BHttpClient* httpClient = qobject_cast<BHttpClient*>(sender());
    httpClient->deleteLater();

    qDebug() << "Server Response in (ms): " << httpClient->getExcutionTime();

}
