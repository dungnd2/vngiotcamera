#include "rtspthread.h"
#include "qffmpeg.h"

RtspThread::RtspThread(QObject *parent) :
    QThread(parent)
{
}

void  RtspThread::run()
{
    if (ffmpeg->_init())
    {
        ffmpeg->_play();
    }
}
