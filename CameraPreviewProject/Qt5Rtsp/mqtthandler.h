#ifndef MQTTHANDLE_H
#define MQTTHANDLE_H

#include <QObject>
#include <QByteArray>
#include <QTimer>
#include <QtMqtt/QtMqtt>

class MQTThandler : public QObject
{
    Q_OBJECT
public:
    explicit MQTThandler(QObject *parent = nullptr);

signals:
    void updateCameraList(const QList<int>);

public slots:
    void initServer(const QString& host, uint16_t port);
    void onGetCameraList(int screenId);

private slots:
    void onBrokerDisconnected();
    void onBrokerConnected();
    void onBrokerPingResponse();
    void onMessageReceived(const QByteArray &message, const QMqttTopicName &topic);

    void clientPublish(const QString &topic, const QString &msg);

private:
    QMqttClient     m_mqttClientLocal;
    QTimer          m_checkQueueConnect;

};

#endif // MQTTHANDLE_H
