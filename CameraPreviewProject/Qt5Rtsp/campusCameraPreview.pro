QT       += core gui network
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CameraStreamPreview
TEMPLATE = app


SOURCES += main.cpp\
    http/bhttpclient.cpp \
    http/httpparams.cpp \
    http/httptool.cpp \
    http/json.cpp \
    httpcaller.cpp \
    mqtthandler.cpp \
    playersettings.cpp \
    videowidget.cpp \
    qffmpeg.cpp \
    rtspthread.cpp

HEADERS  += \
    http/bhttpclient.h \
    http/httpparams.h \
    http/httptool.h \
    http/json.h \
    httpcaller.h \
    mqtthandler.h \
    playersettings.h \
    qffmpeg.h \
    rtspthread.h \
    videowidget.h

FORMS    +=


INCLUDEPATH +=  ffmpeg/include
#LIBS += $$PWD/ffmpeg/lib/libavformat.a \
#        $$PWD/ffmpeg/lib/libavfilter.a \
#        $$PWD/ffmpeg/lib/libswscale.a \
#        $$PWD/ffmpeg/lib/libavdevice.a \
#        $$PWD/ffmpeg/lib/libavcodec.a \
#        $$PWD/ffmpeg/lib/libavutil.a \
#        $$PWD/ffmpeg/lib/libswresample.a \
#        -lz -ldl -llzma -lm \

LIBS += -lavformat\
        -lavfilter\
        -lswscale\
        -lavdevice\
        -lavcodec\
        -lavutil\
        -lswresample\
        -lz -ldl -llzma -lm \

DESTDIR=bin


unix:!macx: LIBS += -L$$PWD/../thirdparty/qtmqtt-dev/lib/ -lQt5Mqtt

#unix:!macx: LIBS += -L$$PWD/../qtmqtt-lib/lib/ -lQt5Mqtt

INCLUDEPATH += $$PWD/../qtmqtt-lib/include
DEPENDPATH += $$PWD/../qtmqtt-lib/include
