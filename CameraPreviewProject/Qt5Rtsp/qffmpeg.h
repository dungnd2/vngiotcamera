#ifndef QFFMPEG_H
#define QFFMPEG_H

#ifndef INT64_C
#define INT64_C
#define UINT64_C
#endif

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>
#include <libswscale/swscale.h>
#include <libavutil/frame.h>
#include <libavutil/pixfmt.h>
}

#include <QObject>
#include <QMutex>
#include <QImage>
#include "rtspthread.h"

class RtspThread;
class QFFmpeg : public QObject
{
    Q_OBJECT
public:
    enum State
    {
        StoppedState,
        PlayingState,
        PausedState
    };

    enum MediaStatus
    {
        UnknownMediaStatus,
        NoMedia,
        LoadingMedia,
        LoadedMedia,
        StalledMedia,
        BufferingMedia,
        BufferedMedia,
        EndOfMedia,
        InvalidMedia
    };

    enum Error
    {
        NoError,
        ResourceError,
        FormatError,
        NetworkError,
        AccessDeniedError,
        ServiceMissingError,
        MediaIsPlaylist
    };



    explicit QFFmpeg(int id, QObject *parent = 0);
    ~QFFmpeg();

    void Play();
    void Stop();

    void SetUrl(QString url)
    {
        this->url=url;
    }

    QString Url() const
    {
        return url;
    }

    int VideoWidth() const
    {
        return videoWidth;
    }

    int VideoHeight()const
    {
        return videoHeight;
    }

    State currentState() const
    {
        return this->_currentState;
    }

    MediaStatus currentMediaState() const
    {
        return this->_mediaStatus;
    }

    void setDisplayWidth(int w) { _displayWidth = w; };
    void setDisplayHeight(int h) { _displayHeight = h; };
    int getDisplayWidth() const { return _displayWidth; };
    int getDisplayHeight() const {return _displayHeight; };


private:
    bool _init();
    void _play();

private:
    QMutex mutex;
    AVPicture  pAVPicture;
    AVFormatContext *pAVFormatContext;
    AVCodecContext *pAVCodecContext;
    AVFrame *pAVFrame;
    SwsContext * pSwsContext;
    AVPacket pAVPacket;

    /* stream informations */
    QString url;
    int videoWidth;
    int videoHeight;
    int videoStreamIndex;

    int _displayWidth;
    int _displayHeight;


    State _currentState;
    MediaStatus _mediaStatus;

    friend class RtspThread;
    RtspThread* _workerThread;

    int _clientId;

signals:
    void GetImage(int id, const QImage &image);

public slots:

};

#endif // QFFMPEG_H
