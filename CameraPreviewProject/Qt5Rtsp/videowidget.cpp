#include "videowidget.h"
#include "playersettings.h"
#include <QPainter>
#include <QDebug>

/**
 * @brief VideoWidget::VideoWidget
 * @param parent
 */
VideoWidget::VideoWidget(QWidget* parent): QOpenGLWidget(parent)
{
    _col = PlayerSettings::getInstance().getColumnCount();
    _row = PlayerSettings::getInstance().getRowCount();

    _ready = false;
}

VideoWidget::~VideoWidget()
{

}

/**
 * @brief VideoWidget::initializeGL
 */
void VideoWidget::initializeGL()
{
    initializeOpenGLFunctions();
    glClearColor(0, 0, 0, 1);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
}

/**
 * @brief VideoWidget::resizeGL
 * @param w
 * @param h
 */
void VideoWidget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
}

/**
 * @brief VideoWidget::paintGL
 */
void VideoWidget::paintGL()
{
    if (_ready)
    {
      QPainter p(this);
      p.setRenderHint(QPainter::HighQualityAntialiasing, true);

      for (int j = 0; j < _row; ++j)
      {
          for (int i=0; i < _col; i++)
          {
              QImage* pi = _images[j*_row + i];

              if (pi)
              {
                  // draw image
                  QRect r(i * (1920/_col) , j* (1080/_row), (1920/_col), (1080/_row));
                  p.drawImage(r, *pi);
              }
          }
      }
    }
}

/**
 * @brief VideoWidget::onUpdateCameraList
 * @param streams
 */
void VideoWidget::onUpdateCameraList(const QList<int> streams)
{
    // update current list
    // 1. stop stream
    this->Stop();

    // 2. update config
    PlayerSettings::getInstance().setStreams(streams);

    //3. play
    this->Run();
}

/**
 * @brief VideoWidget::setImage
 * @param id
 * @param image
 */
void VideoWidget::setImage(int id, const QImage &image)
{
    // qDebug() << "Paint Image from ID = " << id;

    QImage* pimg = _images[id];
    if (pimg)
    {
        *pimg = image;

        if ((id == _col * _row -1) || id == 1 || id == 5 || id == 10)
            QWidget::update();
    }
}

/**
 * @brief VideoWidget::Run
 */
void VideoWidget::Run()
{
    /* it's crash in some case */
    for (int i=0; i < _row * _col; i++)
    {
        QFFmpeg *f = new QFFmpeg(i, this);
        f->setDisplayWidth(1920/6);
        f->setDisplayHeight(1080/6);


        connect(f, SIGNAL(GetImage(int, QImage)),this,SLOT(setImage(int, QImage)));

        QImage *pi = new QImage();

        this->_images[i] = pi;
        this->_fl[i] = f;

        f->SetUrl(PlayerSettings::getInstance().getStream(i));
        f->Play();
    }

    _ready = true;
}

/**
 * @brief VideoWidget::Stop
 */
void VideoWidget::Stop()
{
    if (_ready)
    {
        _ready = false;

        for (int i=0; i < _row * _col; i++)
        {
            QFFmpeg *f = this->_fl[i];

            if (f)
            {
                f->Stop();
                f->deleteLater();
            }
        }
    }
}
