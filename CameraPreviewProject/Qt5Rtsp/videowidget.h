#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QList>
#include <QMap>
#include "qffmpeg.h"

class VideoWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit VideoWidget(QWidget *parent = 0);
    ~VideoWidget() override;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;


public slots:
    void onUpdateCameraList(const QList<int>);
    void setImage(int id, const QImage &image);
    void Run();
    void Stop();

private:
    QMap<int, QFFmpeg*> _fl;
    QMap<int, QImage*> _images;

    int _row;
    int _col;

    bool _ready;
    bool _readyToDisplay;

};

#endif // VIDEOWIDGET_H
