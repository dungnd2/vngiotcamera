#include <QApplication>
#include <QOpenGLWidget>
#include <QScreen>
#include <QDesktopWidget>

#include "playersettings.h"
#include "videowidget.h"
#include "httpcaller.h"
#include "mqtthandler.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    int screenId = PlayerSettings::getInstance().getScreenId();
    QRect screenres = QApplication::desktop()->screenGeometry(screenId);
    VideoWidget w;

    w.move(QPoint(screenres.x(), screenres.y()));
    w.resize(screenres.width(), screenres.height());
    w.showFullScreen();
    w.Run();

    // just testing
    HTTPCaller caller;
    QObject::connect(&caller, &HTTPCaller::updateCameraList, &w, &VideoWidget::onUpdateCameraList);
    caller.onGetCameraList(PlayerSettings::getInstance().getId());

    MQTThandler mqttHandler;
    mqttHandler.initServer("172.26.12.9", 1883);
    QObject::connect(&mqttHandler, &MQTThandler::updateCameraList, &w, &VideoWidget::onUpdateCameraList);


    return a.exec();
}
