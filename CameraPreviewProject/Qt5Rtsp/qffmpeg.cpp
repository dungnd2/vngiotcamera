#include "rtspthread.h"
#include "qffmpeg.h"

#include <QDateTime>
#include <QDebug>

/**
 * @brief QFFmpeg::QFFmpeg
 * @param parent
 */
QFFmpeg::QFFmpeg( int id, QObject *parent) : _clientId(id),
    QObject(parent)
{
    videoStreamIndex=-1;
    av_register_all();
    avformat_network_init();
    pAVFormatContext = avformat_alloc_context();
    pAVFrame=av_frame_alloc();

    _workerThread = new RtspThread(this);
    _workerThread->setffmpeg(this);
}

/**
 * @brief QFFmpeg::~QFFmpeg
 */
QFFmpeg::~QFFmpeg()
{
    if (this->_currentState == QFFmpeg::PlayingState)
    {
        this->_currentState = QFFmpeg::StoppedState;
    }

    if (_workerThread)
    {
        if (_workerThread->isRunning())
        {
            _workerThread->terminate();
            _workerThread->wait();
        }
        _workerThread->deleteLater();
    }

    avformat_free_context(pAVFormatContext);
    av_frame_free(&pAVFrame);
    // FIXME: sws_freeContext(pSwsContext);
}

/**
 * @brief QFFmpeg::_init
 * @return
 */
bool QFFmpeg::_init()
{
    // Open RTSP URL
    AVDictionary *opts = 0;

    // prefer TCP
    av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    int result = avformat_open_input(&pAVFormatContext, url.toStdString().c_str(),NULL, &opts);
    if (result<0)
    {
        qDebug()<< "Open URL:  " << url << " Error" << endl;
        this->_mediaStatus = QFFmpeg::NoMedia;

        return false;
    }

    // Find streams
    result = avformat_find_stream_info(pAVFormatContext,NULL);
    if (result<0)
    {
        qDebug()<<"Sờ chim ơ rô";
        this->_mediaStatus = QFFmpeg::InvalidMedia;
        return false;
    }

    // Find Video stream
    videoStreamIndex = -1;
    for (uint i = 0; i < pAVFormatContext->nb_streams; i++)
    {
        if (pAVFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoStreamIndex = i;
            break;
        }
    }

    if (videoStreamIndex==-1)
    {
        qDebug()<<" Đéo thấy Video sờ chim";
        this->_mediaStatus = QFFmpeg::InvalidMedia;

        return false;
    }

    // Init video codec
    pAVCodecContext = pAVFormatContext->streams[videoStreamIndex]->codec;
    videoWidth = pAVCodecContext->width;
    videoHeight = pAVCodecContext->height;

    if (videoWidth == 0 || videoHeight == 0)
    {
        qDebug()<<" Kích thước sai con mẹ nó rồi";
        return false;
    }

    // Khởi tạo bộ nhớ ảnh
    int w = _displayWidth;
    int h = _displayHeight;

    // Khởi tạo bộ decode
    AVCodec *pAVCodec = avcodec_find_decoder(pAVCodecContext->codec_id);

    pSwsContext = sws_getContext(
                videoWidth, videoHeight, AV_PIX_FMT_YUV420P,
                w, h, AV_PIX_FMT_RGB24,
                SWS_LANCZOS /*SWS_BICUBIC*/, NULL, NULL, NULL);

    // Mở codec
    result = avcodec_open2(pAVCodecContext, pAVCodec, NULL);
    if (result<0)
    {
        qDebug()<<"Codec lỗi mẹ nó rồi";
        this->_mediaStatus = QFFmpeg::InvalidMedia;
        return false;
    }

    qDebug()<<"Open sờ chim ok";
    this->_mediaStatus = QFFmpeg::BufferingMedia;

    return true;
}

/**
 * @brief QFFmpeg::_play
 */
void QFFmpeg::_play()
{
    // Play video nào
    int frameFinished=0;

    // set playing state
    this->_currentState = QFFmpeg::PlayingState;

    // test

    int w = _displayWidth;
    int h = _displayHeight;

    AVFrame* frame2 = av_frame_alloc();
    int num_bytes = avpicture_get_size(AV_PIX_FMT_RGB24, w, h);
    uint8_t* frame2_buffer = (uint8_t *)av_malloc(num_bytes*sizeof(uint8_t));
    avpicture_fill((AVPicture*)frame2, frame2_buffer, AV_PIX_FMT_RGB24, w, h);


    while (this->_currentState == QFFmpeg::PlayingState)
    {
        // Đọc từng Frame vô
        if (av_read_frame(pAVFormatContext, &pAVPacket) >= 0)
        {
            if(pAVPacket.stream_index==videoStreamIndex)
            {
                // qDebug() << " Video Sờ chim: "<<QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss");
                avcodec_decode_video2(pAVCodecContext, pAVFrame, &frameFinished, &pAVPacket);

                if (frameFinished)
                {
                    //mutex.lock();

                    sws_scale(pSwsContext,
                              (const uint8_t* const *) pAVFrame->data, pAVFrame->linesize, 0, videoHeight,
                              frame2->data, frame2->linesize);


                    //Render nào
                    QImage image(frame2->data[0], w, h, QImage::Format_RGB888);
                    emit GetImage(_clientId, image);
                    //mutex.unlock();
                }
            }
        }
        av_free_packet(&pAVPacket);
    }

    av_frame_free(&frame2);
}

/**
 * @brief QFFmpeg::Play
 */
void QFFmpeg::Play()
{
    // play from thread
    _workerThread->start();
}

/**
 * @brief QFFmpeg::Stop
 */
void QFFmpeg::Stop()
{
    if (this->_currentState == QFFmpeg::PlayingState)
    {
        this->_currentState = QFFmpeg::StoppedState;

        if (_workerThread->isRunning())
        {
            _workerThread->terminate();
            _workerThread->wait();
        }

    }

}
