#include "mqtthandler.h"
#include "playersettings.h"

#define DEFAULT_KEEPALIVE 10

MQTThandler::MQTThandler(QObject *parent) : QObject(parent)
{

}

void MQTThandler::initServer(const QString &host, uint16_t port)
{
    m_mqttClientLocal.setHostname(host);
    m_mqttClientLocal.setPort((quint16) port);
    m_mqttClientLocal.setProtocolVersion(QMqttClient::MQTT_3_1);
    m_mqttClientLocal.setKeepAlive(DEFAULT_KEEPALIVE);

    connect(&m_mqttClientLocal, &QMqttClient::connected, this, &MQTThandler::onBrokerConnected);
    connect(&m_mqttClientLocal, &QMqttClient::disconnected, this, &MQTThandler::onBrokerDisconnected);
    connect(&m_mqttClientLocal, &QMqttClient::messageReceived, this, &MQTThandler::onMessageReceived);
    connect(&m_mqttClientLocal, &QMqttClient::pingResponseReceived, this, &MQTThandler::onBrokerPingResponse);

    m_mqttClientLocal.connectToHost();

}

/**
 * @brief MQTThandler::onGetCameraList
 * @param screenId
 *
 * Lấy danh sách camera cho mỗi screen cụ thể (async)
 */
void MQTThandler::onGetCameraList(int screenId)
{

}

void MQTThandler::onBrokerDisconnected()
{
    qDebug() << "Local MQTT Server disconnected ..." << m_mqttClientLocal.error();

    // try reconnect
    qDebug() << "cONNECT TO Local MQTT Server";
    QTimer::singleShot(100, &m_mqttClientLocal, SLOT(QMqttClient::connectToHost()));

}

void MQTThandler::onBrokerConnected()
{
    qDebug() << "Local MQTT Server connected, trying subscribe";

    QString clientServerTopic = "vng/camera-cloud/campus/client-view-config/server";

    m_mqttClientLocal.subscribe(clientServerTopic);

}

void MQTThandler::onBrokerPingResponse()
{
    qDebug() << "Local MQTT Server Ping received";

}

/**
 * @brief MQTThandler::clientPublish
 * @param topic
 * @param msg
 */
void MQTThandler::clientPublish(const QString &topic, const QString &msg)
{
    m_mqttClientLocal.publish(topic, msg.toUtf8());
}

/**
 * @brief MQTThandler::onMessageReceived
 * @param message
 * @param topic
 */
void MQTThandler::onMessageReceived(const QByteArray &message, const QMqttTopicName &topic)
{
    qDebug() << "message "<< message << "topic " << topic.name();

    // check validate parametters
    QString         jsonmessage(message);
    if (jsonmessage.length() <=0)
    {
        qDebug() << "invalid message";
        return;
    }

    QJsonDocument   jsd = QJsonDocument::fromJson(jsonmessage.toUtf8());
    if (jsd.isNull() || jsd.isEmpty())
    {
        qDebug() << "Invalide JsonDocument";
        return;
    }

    QJsonObject     jso = jsd.object();
    if (jso.isEmpty())
    {
        qDebug() << "Invalide json object";
        return;
    }

    QString command = jso["cm"].toString();

    qDebug() << "Command: " << command;



    /* Load all screen */
    if (0 == command.compare("screen_load"))
    {
        QJsonObject jdo = jso["dt"].toObject();

        int client_id = jdo["client_id"].toInt();
        if (client_id != PlayerSettings::getInstance().getId())
            return;

        QJsonArray cams = jdo["cams"].toArray();

        QList<int> camList;

        foreach(QJsonValue cam, cams)
        {
            if (cam.isObject())
            {
                int camid = cam.toObject()["id"].toInt();

                qDebug() << camid;
                camList.push_back(camid);
            }
        }

        /* valid screen-size */
        if (camList.size() == PlayerSettings::getInstance().getStreamCount())
        {
            emit updateCameraList(camList);
        }
    }


}

