#ifndef PLAYERSETTINGS_H
#define PLAYERSETTINGS_H

#include <QObject>
#include <QSettings>
#include <QStringList>
#include <QRect>
#include <QVector>
#include <QMap>

/**
 * @brief The PlayerSettings class: load config from current folder
 */
class PlayerSettings
{
public:
    PlayerSettings();
    static PlayerSettings &getInstance();

    int getStreamCount();
    int getRowCount();
    int getColumnCount();

    int getScreenId();
    int getScreenWidh();
    int getScreenHeight();

    QString getPlayerName();
    int getId() const { return this->_playerId; };
    QString getStream(int count);

    int getStreamById(int id) const
    {
        return _totalStreams[id];
    }

    void setStreams(const QList<int> streams);

private:
    void load();
    void store();

    void loadNativeStreams();

private:
    static PlayerSettings* _instance;

    int _columnCount;
    int _rowCount;
    int _screenNumber;

    int _loadOpt;

    int _beginId;
    int _beginFailedId;

    QRect _screenRect;

    QString _playerName;
    int     _playerId;
    QStringList _playerUrls;
    QVector<int> _totalStreams;
    QMap<int, QString> _nativeStreams;

    QString _masterServer;
    QString _backupServer;
};

#endif // PLAYERSETTINGS_H
