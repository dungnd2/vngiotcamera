cmake_minimum_required(VERSION 3.10)
project(Test)

find_package(Qt5Core REQUIRED)
# find_package(Qt5Networks REQUIRED)

message(Qt::Core)

add_executable(main main.cpp)

find_library(LIB NAMES avcodec PATHS /home/dung/Documents/CameraPreviewProject/Qt5Rtsp/ffmpeg/lib)

message(${LIB})
target_link_libraries(main PRIVATE ${LIB})