#!/bin/sh
QT_VERSION=5.12.5
export QT_VERSION
QT_VER=5.12
export QT_VER
QT_VERSION_TAG=5125
export QT_VERSION_TAG
QT_INSTALL_DOCS=/home/compiler/Qt5.13.0/Docs/Qt-5.13.0
export QT_INSTALL_DOCS
BUILDDIR=/home/compiler/working/gps/build-qtmqtt-Desktop_Qt_5_13_0_GCC_64bit-Debug/src/mqtt
export BUILDDIR
exec /home/compiler/Qt5.13.0/5.13.0/gcc_64/bin/qdoc "$@"
